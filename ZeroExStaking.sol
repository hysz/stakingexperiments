pragma solidity 0.5.0;


contract ZeroExStakingContract {



    ////////////////////////////////
    /////// Asset Management ///////
    ////////////////////////////////

    /**
        There are three “Tokens”:
        - Staked ZRX     — ZRX that has been staked by a Market Maker.
        - Delegated ZRX  — ZRX that has been delegated to a Market Maker
        - Future ZRX     - ZRX that will be owned in the future
     */

    // marketMakerId => (epoch => balance)
    mapping (bytes32 => (uint256 => uint256)) stakedZRX;        // rolls over to next epoch

    // marketMakerId => (epoch => balance)
    mapping (bytes32 => (uint256 => uint256)) delegatedZRX;     // resets each epoch

    // holder => (epoch => balance)
    mapping (address => (uint256 => uint256)) futureZRX;        // gets withdrawn each epoch then resets

    // epoch => holders
    mapping (uint256 => address[]) futureZRXHolders;             // resets each epoch

    function getStakedZRXBalance(bytes32 marketMakerId, uint256 epoch) {}
    function getDelegatedZRXBalance(bytes32 marketMakerId, uint256 epoch) {}
    function getFutureZRXBalance(address holder, uint256 epoch) {}


    ////////////////////////////////
    /////// Stake Delegation ///////
    ////////////////////////////////   

    // Interface for trading on 0x as an 1155 token
    // By default allowances are set for the 0x ERC1155 Proxy
    /**
        StakeDelegationOrder:
        {
            // Maker
            maker: ZeroExStakingContract
            makerAssetAmount: 1 Future ZRX
            makerAssetData:
            {
                contract: ZeroExStakingContract
                ids: [MarketMakerId]
                values: [1]
                data: {
                    marketMakerCost: 0.001 Staked ZRX
                }
            }
            
            // Taker
            taker: 0x0 // A ZRX Delegator
            takerAssetAmount: 0.999 ZRX
            takerAssetData:
            {
                contract: ZRXToken
            }
        }
     */
    function safeBatchTransferFrom(
        address from,
        address to,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes data
    ) 
        external
    {
        if (futureZRX[nextEpoch][to] == 0) {
            futureZRXHolders[nextEpoch].push(to);
        }

        require(ids.length == 1);
        require(values.length == 1);
        
        bytes32 makerId = bytes32(ids[0]);
        require(isMakerRegistered(makerId));

        futureZRX[nextEpoch][to] += values[0];
        delegatedZRX[nextEpoch][makerId] += values[0];
        stakedZRX[nextEpoch][makerId] -= data.marketMakerCost;
    }


    ////////////////////////////////
    /////// Stake Management ///////
    ////////////////////////////////   

    /**
        stakeOrder:
        {
            // Maker
            maker: MarketMaker
            makerAssetAmount: 1000 ZRX
            makerAssetData:
            {
                contract: ZRXToken
            }
            
            // Taker
            taker: ZeroExStakingContract
            takerAssetAmount: 1000 Staked ZRX
            takerAssetData:
            {
                contract: ZeroExStakingContract
                ids: [MarketMakerId]
                values: [1000]
                data: <>
            }

            // Must be sent by the staking contract
            sender: ZeroExStakingContract
        }

     */
    function stakeZRX(ZeroExOrder stakeOrder) {
        // sanity check -- 1:1 ratio of ZRX to Staked ZRX
        
        // fill order. contract now holds makerAssetAmount ZRX.

        // stakedZRX[maker][nextEpoch] += stakeOrder.makerAssetAmount
    }


    function unstakeZRX(uint256 amountToUnstake, address receiver) {
        // sanity check on amountToUnstake

        // add receiver to futureZRXHolders at nextEpoch if their futureZRXBalance for nextEpoch is 0.

        // stakedZRX[maker][nextEpoch] -= amountToUnstake

        // withdraw if staked ZRX is not being used by current epoch
        // else, futureZRX[receiver][nextEpoch] += amountToUnstake
    }


    ////////////////////////////////
    /////// Epoch Management ///////
    ////////////////////////////////

    uint epochDuration;
    uint256 currentEpoch;
    uint256 currentEpochStart;
    bool rolloverInProcess;
    uint256 numberOfMakersRolledOver;
    uint256 numberOfFutureZRXHoldersPaidOut;

    // Rollovers:
    // We rollover state from "next" epoch to "current epoch"
    // The general workflow of rollovers:
    // Trading happens on epoch + 1
    // Payouts happen on epoch - 1
    //
    // Workflow:
    // 1. Start staking / delegating for Epoch 0.
    // 2. Stop staking / delegating for Epoch 0.
    // 3. Rollover StakeZRX from epoch 0 to Epoch 1.
    // 4. Epoch 0 starts.
    //
    // 5. Start staking / delegating for Epoch 1.
    // 6. Stop staking / delegating for Epoch 1.
    // 7. Rollover StakeZRX from epoch 1 to Epoch 2.
    // 8. Payout FutureZRX holders from Epoch 0.  
    // 9. Epoch 0 ends. Epoch 1 begins.
    //
    // 10. Start staking / delegating for Epoch 2.
    // 11. Stop staking / delegating for Epoch 2.
    // 12. Rollover StakeZRX from epoch 2 to Epoch 3.
    // 13. Payout FutureZRX holders from Epoch 1.  
    // 14. Epoch 1 ends. Epoch 2 begins.
    //
    // ...
    //
    //
    // Thought - should there be some reward/incentive for calling these rollover functions?

    function getCurrentEpoch()
    {
        // return epoch
    }    

    function startEpochRollover()
    {
        // check that epoch has completed
        // set rolloverInProcess to 1
    }

    function finishEpochRollover()
    {
        // check that epoch rollover has completed

        // increment currentEpoch and set currentEpochStart

        // set rolloverInProcess to 0
    }

    function rolloverStakedZRX(uint256 numberOfMakers)
    {
        // check that rolloverInProcess

        // rollover staked ZRX from epoch + 1 to epoch + 2 for market makers [numberOfMakers .. numberOfMakersRolledOver + numberOfMakers]
        
        // increment numberOfMakersRolledOver
    }

    function payoutFutureZRXHolders(uint256 numberOfHolders)
    {
        // check that rolloverInProcess

        // payout ZRX holders from current epoch for holders [numberOfFutureZRXHoldersPaidOut .. numberOfFutureZRXHoldersPaidOut + numberOfHolders]
        
        // increment numberOfFutureZRXHoldersPaidOut
    }
}


    ////////////////////////////////
    /////// Maker Management ///////
    ////////////////////////////////   

    // id of next market maker
    uint256 nextMarketMakerId;

    // marketMakerId => (epoch => addresses)
    mapping (bytes32 => (uint256 => address[])) marketMakers;

    // should there be some deposit to prevent too many from registering?
    // we have the maker register their addresses so we can gauge their network contribution during a given epoch.
    function registerMarketMaker(address[] addresses, bytes[] signatures) {
        // check that each address has been signed
        
        // perhaps (?) have a signature type for a contract 

        // store addresses under nextMarketMakerId

        // increment nextMarketMakerId
    }

    function addMarketMakerAddresses(uint256 marketMakerId, address[] addresses, bytes[] signatures) {
        // updates for next epoch
    }

    function removeMarketMakerAddresses(uint256 marketMakerId, address[] addresses) {
        // updates for next epoch
    }

    function getMarketMakerAddresses(uint256 marketMakerId, uint256 epoch) returns (address[]) {}








    ////////////////////////////////
    /////// ALTERNATIVE Stake Delegation -- 0x orders only ///////
    ////////////////////////////////   

    // Interface for trading on 0x as an 1155 token
    // By default allowances are set for the 0x ERC1155 Proxy
    /**
        StakeDelegationOrder:
        {
            // Maker
            maker: ZeroExStakingContract
            makerAssetAmount: 1 Future ZRX
            makerAssetData:
            {
                contract: ZeroExStakingContract
                ids: [MarketMakerId]
                values: [1]
                data: {
                    marketMakerCost: 0.001 Staked ZRX
                }
            }
            
            // Taker
            taker: 0x0 // A ZRX Delegator
            takerAssetAmount: 0.999 ZRX
            takerAssetData:
            {
                contract: ZRXToken
            }
        }

        StakeOrder:
        {
            // Maker
            maker: ZeroExStakingContract
            makerAssetAmount: 1 Staked ZRX
            makerAssetData:
            {
                contract: ZeroExStakingContract
                ids: [NilMarketMaker]
                values: [1]
                data: {}
            }
            
            // Taker
            taker: 0x0 // A Market Maker
            takerAssetAmount: 1 ZRX
            takerAssetData:
            {
                contract: ZRXToken
            }
        }
        ^^
        we check that makerAssetAmount equals takerAssetAmount in isValidSignature

        UnStakeOrder:
        {
            // Maker
            maker: MarketMaker
            makerAssetAmount: 1 Staked ZRX
            makerAssetData:
            {
                contract: ZeroExStakingContract
                ids: [MarketMakerId]
                values: [1]
                data: {}
            }
            
            // Taker
            taker: 0x0 MarketMaker
            takerAssetAmount: 1 Future ZRX // or 1 ZRX if stake isnt being used in current Epoch
            takerAssetData:
            {
                contract: NotToken
            }
        }
        ^^
        we check that makerAssetAmount equals takerAssetAmount in isValidSignature
     */
    function safeBatchTransferFrom(
        address from,
        address to,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes data
    ) 
        external
        onlyCallableBy0xProxy
    {
      // call delegateStake / stakeZRX / unstakeZRX
       
    }